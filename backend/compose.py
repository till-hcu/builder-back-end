from xml.dom import minidom as MD
from os import path as Path
import json

basePath = "./assets/xml"
aggregation = {}
mergeableAttributes = ["filters", "inputs"]

with open("./assets/json/config_example.json") as example:
    aggregation = json.load(example)

def loadXml(path):
    xml_path = Path.join(basePath, path)
    return MD.parse(xml_path)

def matchParams(key, params):
    for param in params:
        if "keys" in param:
            if key in param["keys"]:
                return param
        if "key" in param:
            if param["key"] == key:
                return param
    return None

def injectDataSource(inputParam, xml):
    if "featureType" in inputParam["dataSource"] and "featureNS" in inputParam["dataSource"]:
        featureType = inputParam["dataSource"]["featureType"]
        featureNS = inputParam["dataSource"]["featureNS"]
        wfsQuery = xml.getElementsByTagName("wfs:Query")[0]
        wfsQuery.attributes["typeName"].value = featureNS + ":" + featureType
    else:
        geojson = json.dumps(inputParam["dataSource"], )
        cdata = '<![CDATA[' + geojson + ']]>'

        wfsRef = xml.getElementsByTagName("wps:Reference")[0]
        xml.removeChild(wfsRef)

        doc = xml.ownerDocument
        dataNode = doc.createElement("wps:Data")
        complexDataNode = doc.createElement("wps:ComplexData")
        complexDataNode.setAttribute("mimeType", "application/json")
        textNode = doc.createTextNode(cdata)

        complexDataNode.appendChild(textNode)
        dataNode.appendChild(complexDataNode)
        xml.appendChild(dataNode)

    return xml

def injectAttribute(inputParam, xml):
    literal = xml.getElementsByTagName("wps:LiteralData")[0]
    if "value" in inputParam:
        literal.firstChild.replaceWholeText(inputParam["value"])
    return xml

def injectInput(config, xml, key, inputType):
    inputs = config["inputs"]
    inputParam = matchParams(key, inputs)

    if inputParam is None:
        return xml

    if inputType == "dataSource":
        xml = injectDataSource(inputParam, xml)
    if inputType == "attribute":
        xml = injectAttribute(inputParam, xml)

    return xml

def injectFilter(config, xml, key):
    filters = config["filters"]
    filterParam = matchParams(key, filters)

    if filterParam is not None and filterParam["value"] != None and len(filterParam["value"]) > 0:
        cqlFilterNode = xml.getElementsByTagName("wps:ComplexData")[0]
        cqlFilterNode.firstChild.replaceWholeText(filterParam["value"])
    else:
        xml.parentNode.removeChild(xml)

    return xml

def injectParams(config, xml):
    inputFields = xml.getElementsByTagName("wps:Input")

    for el in inputFields:
        try:
            key = el.attributes["key"].value
            inputType = el.attributes["inputType"].value
            if inputType == "filter":
                el = injectFilter(config, el, key)
            else:
                el = injectInput(config, el, key, inputType)
        except KeyError:
            print("Input does not specify input-key and/or input-type. Please check XML Template")
    
    return xml

def composeXml(config, xml=None):
    if xml == None:
        xml = loadXml(config["requestBody"])
    else:
        xml = MD.parseString(xml)

    xml = injectParams(config, xml)

    return xml.toxml()

def overrideParams(new_param, old_params):
    if new_param is not None:
        for old_param in old_params:
            override = False
            if "keys" in new_param:
                if "key" in old_param:
                    if old_param["key"] in new_param["keys"]:
                        override = True
                if "keys" in old_param:
                    if len(list(set(new_param["keys"]).intersection(old_param["keys"]))) > 0:
                        override = True
            if "key" in new_param:
                if "key" in old_param:
                    if new_param["key"] == old_param["key"]:
                        override = True
                if "keys" in old_param:
                    if new_param["key"] in old_params["keys"]:
                        override = True
            if override:
                old_params.remove(old_param)
    return old_params

def joinConfig(config, request):
    new_config = None

    if request.method == "POST":
        new_config = request.get_json()
    
    if new_config != None:
        for attr in mergeableAttributes:
            if attr in new_config:
                for param in new_config[attr]:
                    config[attr] = overrideParams(param, config[attr])
                    config[attr].append(param)

    return config

if __name__ == "__main__":
    composeXml(aggregation)
    

