import requests
import json
import os
from distutils import util as distutils

geoserverUrl = 'https://csl-lig.hcu-hamburg.de/geoserver/ows'
GEOSERVER_LOCAL = os.environ.get("GEOSERVER_LOCAL")
geoserverLocal = None

if GEOSERVER_LOCAL is not None:
    geoserverLocal = distutils.strtobool(GEOSERVER_LOCAL)
    if geoserverLocal:
        geoserverUrl = 'http://geoserver:8080/geoserver/ows'

def execute(xml, config={}, params={"request": "EXECUTE", "version": "1.0.0", "service": "WPS"}):
    headers = {'Content-Type': 'text/xml'}
    url = geoserverUrl
    if "url" in config and geoserverLocal is None:
        url = config["url"]
    if "params" in config:
        params = config["params"]

    print("Making WPS Request with identifier", config["processIdentifier"], "to", url)
    response = requests.post(url, params=params, headers=headers, data=xml.encode("utf-8"), verify=False)
    return json.loads(response.content)