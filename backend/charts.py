import random
import json

_backgroundColors = None

def getRandomColor (clamp=(100, 255), alpha=1.0):
    r = round((random.random() * (clamp[1] - clamp[0])) + clamp[0])
    g = round((random.random() * (clamp[1] - clamp[0])) + clamp[0])
    b = round((random.random() * (clamp[1] - clamp[0])) + clamp[0])
    return f"rgba({r}, {g}, {b}, {alpha})"

def getXLabels(result, separator="/"):
    indices = (1, len(result["GroupByAttributes"]))[isinstance(result["GroupByAttributes"], list)]

    return [''.join(str(attr) for attr in el[:indices]) for el in result["AggregationResults"]]


def getYLabel(label, result, separator="/"):
    return result["AggregationAttribute"] + separator + label

def getBarDataStyles(data, datasetCount, datasetIndex, customColors=[]):
    colors = None

    if datasetCount == 1:
        colors = []
        for i,v in enumerate(data):
            if len(customColors == 1):
                colors.append(customColors[0])
            elif isinstance(customColors, dict) and v in customColors:
                colors.append(customColors[v])
            elif isinstance(customColors, list) and i < len(customColors):
                colors.append(customColors[i])
            else:
                colors.append(getRandomColor())
    elif isinstance(customColors, list) and datasetIndex < len(customColors):
        colors = customColors[datasetIndex]
    else:
        colors = getRandomColor()
    
    return colors

def getPieDataStyles(data, datasetCount, datasetIndex, customColors=[]):
    global _backgroundColors
    colors = None

    if _backgroundColors is not None:
        colors = _backgroundColors
    else:
        colors = []
        for i,v in enumerate(data):
            if isinstance(customColors, dict) and v in customColors:
                colors.append(customColors[v])
            elif isinstance(customColors, list) and i < len(customColors):
                colors.append(customColors[i])
            else:
                colors.append(getRandomColor())
        _backgroundColors = colors
    
    return colors

def getDataStyles(data, datasetCount, datasetIndex, chartType, opts):
    backgroundColor = []
    
    if "backgroundColor" not in opts:
        opts["backgroundColor"] = None

    if chartType == "bar":
        backgroundColor = getBarDataStyles(data, datasetCount, datasetIndex, opts["backgroundColor"])
    elif chartType == "line":
        backgroundColor = getBarDataStyles(data, datasetCount, datasetIndex, opts["backgroundColor"])
    elif chartType == "pie":
        backgroundColor = getPieDataStyles(data, datasetCount, datasetIndex, opts["backgroundColor"])
    elif chartType == "polarArea":
        backgroundColor = getPieDataStyles(data, datasetCount, datasetIndex, opts["backgroundColor"])

    return {
        **opts,
        "backgroundColor": backgroundColor
    }


def getDatasets(result, datasetOptions, chartType="bar"):
    datasets = []
    opts = {}
    cullIndices = (1, len(result["GroupByAttributes"]))[isinstance(result["GroupByAttributes"], list)]

    if (datasetOptions is not None):
        opts = datasetOptions

    for i,_label in enumerate(result["AggregationFunctions"]):
        label = getYLabel(_label, result)
        data = [el[cullIndices+i] for el in result["AggregationResults"]]
        dataStyles = getDataStyles(data, len(result["AggregationFunctions"]), i, chartType, opts)
        dataset = {
            "label": label,
            "data": data,
            **dataStyles
        }

        datasets.append(dataset)

    return datasets

def parseResults (results, chartType, datasetOptions):
    # print(json.dumps(getXLabels(results), indent=4))
    # print(json.dumps(getDatasets(results, datasetOptions, chartType), indent=4))
    return {
        "labels": getXLabels(results),
        "datasets": getDatasets(results, datasetOptions, chartType)
    }

if __name__ == '__main__':
    print("module compiles")
    # testing here
