import json
from flask import Flask, request, jsonify
from flask_cors import CORS
import requests
import xml.etree.ElementTree as ET
from database.database import saveJSON, load, loadAll, deleteJSON
from backend.compose import composeXml, joinConfig
from backend.wps import execute, geoserverUrl
from backend.charts import parseResults
import os
from distutils import util as distutils

app = Flask(__name__)
CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

@app.route('/executeQuery', methods=["POST"])
def executeQuery():
    query = request.get_json()
    config = query["aggregation"]
    chart = query["chart"]
    requestXml = composeXml(config)
    result = {
        "aggregation": execute(requestXml, config=config),
        "chart": chart
    }

    response = app.response_class(
        response=json.dumps(result),
        status=200,
        mimetype='application/json'
    )

    return response


@app.route('/getJSON', methods=["GET"])
def getJSON():
    # Example call: curl -X GET http://127.0.0.1:5000/getJSON?hash=RvDQus7ubBlBcAeu
    hash = request.args.get('hash')
    table = request.args.get('table')
    if table is None:
        table = "CHARTS"
    db_entry = load(hash, table)
    if db_entry is not None:
        result = db_entry
        response = app.response_class(
            response=json.dumps(result),
            status=200,
            mimetype='application/json'
        )
    else:
        response = app.response_class(
            response="JSON does not exist",
            status=404,
            mimetype='text/plain'
        )
    return response


@app.route('/getResults', methods=["GET", "POST"])
def getResults():
    hash = request.args.get('hash')
    query = load(hash)
    if query is not None:
        config = joinConfig(query["aggregation"], request)
        chart = query["chart"]
        requestXml = composeXml(config)

        result = {
            "aggregation": execute(requestXml, config=config),
            "chart": chart
        }
        response = app.response_class(
            response=json.dumps(result),
            status=200,
            mimetype='application/json'
        )
    else:
        response = app.response_class(
            response="Could not retrieve Request from DB",
            status=404,
            mimetype='text/plain'
        )

    return response

@app.route('/getChart', methods=["GET", "POST"])
def getChart():
    hash = request.args.get('hash')
    query = load(hash)

    if query is not None:
        config = joinConfig(query["aggregation"], request)
        chartDef = query["chart"]
        requestXml = composeXml(config)
        result = execute(requestXml, config=config)

        chart = {
            "chartData": parseResults(result, chartDef["chartType"], chartDef["chartOptions"]["dataset"]),
            "chartOptions": chartDef["chartOptions"],
            "chartType": chartDef["chartType"],
            "hash": hash
        }
        response = app.response_class(
            response=json.dumps(chart),
            status=200,
            mimetype='application/json'
        )
    else:
        response = app.response_class(
            response="Could not retrieve Request from DB",
            status=404,
            mimetype='text/plain'
        )
    
    return response

@app.route('/getAllQueries', methods=["GET"])
def getAllQueries():
    # Example call: curl -X GET http://127.0.0.1:5000/getAllResults
    results = loadAll()
    response = app.response_class(
        response=json.dumps(results),
        status=200,
        mimetype='application/json'
    )
    return response


@app.route('/saveQuery', methods=["POST"])
def saveQuery():
    # Example call: curl -H Content-type:application/json -X POST -d@test.json http://127.0.0.1:5000/saveJSON
    # With test.json in the same folder
    json = request.get_json()
    hash = saveJSON(json, "CHARTS")
    response = app.response_class(
            response=hash,
            status=201,
            mimetype='text/plain'
        )
    return response


@app.route('/deleteQuery', methods=["GET"])
def deleteQuery():
    hash = request.args.get("hash")
    deleteJSON(hash, "CHARTS")
    return 'success'


@app.route('/getAllFilters', methods=["GET"])
def getAllFilters():
    # Example call: curl -X GET http://127.0.0.1:5000/getAllResults
    results = loadAll("FILTERS")
    response = app.response_class(
        response=json.dumps(results),
        status=200,
        mimetype='application/json'
    )
    return response


@app.route('/saveFilter', methods=["POST"])
def saveFilter():
    json = request.get_json()
    hash = saveJSON(json, "FILTERS")
    response = app.response_class(
            response=hash,
            status=201,
            mimetype='text/plain'
        )
    return response


@app.route('/deleteFilter', methods=["GET"])
def deleteFilter():
    hash = request.args.get("hash")
    deleteJSON(hash, "FILTERS")
    return 'success'


if __name__ == '__main__':
    isDocker = os.getenv("DOCKER")
    print("Geoserver is running at", geoserverUrl)
    if isDocker:
        print("Environment is Docker")
        app.run(host='0.0.0.0', debug=True, threaded=True)
    else:
        print("Environment is NOT Docker")
        app.run(host= '127.0.0.1', debug=True, threaded=True)