import json
import sqlite3
import random, string
import hashlib
import os

## Change to local paths - I couldnt fix this!!!
dbPath = 'database/chartsdb.db'
jsonPaths = {
    "CHARTS": 'database/chart-json/{}.json',
    "FILTERS": 'database/filters-json/{}.json'
}

def connect():
    con = sqlite3.connect(dbPath)
    if con:
        # get the count of tables with the name
        cur = con.cursor()

        for key in jsonPaths.keys():
            cur.execute(''' SELECT count(name) FROM sqlite_master WHERE type='table' AND name=? ''', (key,))
            # if the count is 1, then table exists
            if cur.fetchone()[0] == 1:
                print('Table exists.')
            else:
                print("creating table", key)
                createTable(con, key)
    return con

def load(hash, table="CHARTS"):
    con = connect()
    cur = con.cursor()
    cur.execute("SELECT * FROM " + table + " WHERE  hash=?", (hash,))
    result = cur.fetchone()
    if result:
        try:
            with open(result[2], encoding="utf-8") as config:
                config_json = json.load(config)
                return config_json
        except FileNotFoundError:
            print("config not in DB")
        
    else:
        return None


def loadAll(table="CHARTS"):
    con = connect()
    cur = con.cursor()
    cur.execute("SELECT * FROM " + table)
    results = cur.fetchall()
    storedQueries = []
    for db_entry in results:
        if os.path.exists(db_entry[2]):
            with open(db_entry[2], encoding="utf-8") as config:
                config_json = json.load(config)
                storedQueries.append((db_entry[1], config_json))
                
    return storedQueries


def saveJSON(chartData, table="CHARTS"):
    con = connect()
    cur = con.cursor()
    _hash = createHash(chartData, False)
    existing = load(_hash)
    if not existing:
        filePath = saveFile(_hash, chartData, jsonPaths[table])
        sql = 'INSERT INTO ' + table + ' (hash, path) values(?, ?)'
        cur.execute(sql, (_hash, filePath))
        con.commit()
        con.close()
    return _hash

def deleteJSON(hash, table="CHARTS"):
    con = connect()
    cur = con.cursor()
    cur.execute("SELECT * FROM " + table + " WHERE  hash=?", (hash,))
    result = cur.fetchone()
    if result:
        filePath = result[2]
        if os.path.exists(filePath):
            os.remove(filePath)
    cur.execute("DELETE FROM " + table + " WHERE hash=?", (hash,))

def createHash(chartData=None, isRandom=True):
    if not isRandom and chartData:
        _hash = hashlib.md5(json.dumps(chartData).encode('utf-8'))
        _hash = str(_hash.hexdigest())
    else:
        _hash = ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(16))

    return _hash

def createTable(con, table):
    cur = con.cursor()
    cur.execute("""
                CREATE TABLE if not exists """ + table + """ (
                    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                    hash TEXT,
                    path TEXT,
                    Timestamp DATETIME DEFAULT CURRENT_TIMESTAMP
                );
            """)
    con.commit()

def saveFile(hash, data, dirPath):
    filePath = dirPath.format(hash)

    with open(filePath, 'w', encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False, indent=4)

    return filePath
