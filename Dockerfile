FROM python:3.6.1-alpine
WORKDIR /project
ENV DOCKER=True
ARG GEOSERVER_LOCAL
ADD . /project
RUN pip install -r requirements.txt
EXPOSE 5000
CMD ["python","main.py"]