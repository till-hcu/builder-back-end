# Prototype Dashboard-Builder Backend

Teile dieser Dokumentation sind der schriftlichen Ausarbeitung der Machbarkeitsstudie Dashboard-Builder entnommen.
Diese befindet sich in folgendem Repository: https://bitbucket.org/till-hcu/machbarkeitsstudie/src/master/


Das Backend des Prototyps übernimmt (1) Speicherung und Abruf der Konfigurationen mittels der Hashes, (2) das Erzeugen der WPS RequestBodies, (3) die Anfrage des WPS, (4) das Formattieren der Ergebnisse in chart-js kompatible Datenobjekte und (5) die Auslieferung der Daten. Es ist in Python Flask geschrieben und mittels der Docker-Konfiguration leicht zu deployen. Die Anleitung liegt dem Repository bei.
Als Datenbank verwendet der Prototyp eine lokale Sqlite3 Instanz, speichert die Konfigurationen jedoch primär als einzelne Dateien mit dem Hash als Dateinamen (z.B. f3a066faf06a41a7c18b0e5ad21c2859.json) im lokalen FileSystem ab. Das Repository beinhaltet außerdem die XML Templates für die RequestBodies, welche in der Aggregations-Beschreibung referenziert werden (s. 4.2.1 d. Machbarkeitsstudie, https://bitbucket.org/till-hcu/docker-compose)

## Routes

Das Backend bietet folgende Routen, welche über HTTP Requests angesprochen werden können:

* _/executeQuery_ (POST): Führt den WPS basierend auf einer im Body mitgelieferten Aggregationskonfiguration aus und liefert das fertige Chart zurück. Wird primär für die Vorschau verwendet.
* _/getJSON_ (GET ): Ruft die Konfiguration basierend auf den URL Parametern hash und table („CHARTS“ oder „FILTER“) ab.
* _/getResults_ (GET, POST): Führt den WPS basierend auf dem URL Parameter hash aus und liefert das Ergebnis-Objekt des WPS zurück. Der Request kann auch  optional als POST-Request ausgeführt werden, indem ein (Teil-) Konfigurationsobjekt mitgegeben wird. Dieses überschreibt bei der Erstellung  des XML-RequestBodies die jeweiligen Attribute aus dem Datenbankeintrag (Relevant für die Filterung von Charts). 
* _/getChart_ (GET, POST): Analog zu 3., liefert aber ein chart-js kompatibles Objekt zurück.
* _/getAllQueries_ (GET): Ruft alle in der Datenbank gespeicherten Chart/Aggregations-Konfigurationen ab.
* _/getAllFilters_ (GET): Ruft alle in der Datenbank gespeicherten Filter-Konfigurationen ab,
* _/saveQuery_ (POST): Hashed und speichert die Aggregations/Chart-Konfiguration in der Datenbank. Liefert den Hash zurück.
* _/deleteQuery_ (DELETE); Löscht eine Konfiguration basierend auf dem URL Parameter hash aus der Datenbank.
* _/saveFilter_ (POST): Hashed und speichert die Filter-Konfiguration in der Datenbank. Liefert den Hash zurück.
* _/deleteFilter_ (DELETE); Löscht einen Filter basierend auf dem URL Parameter hash aus der Datenbank.

## Erzeugung der Charts
Die Erzeugung der eigentlichen Diagramme aus den Ergebnissen (beschrieben in 4.2.3.) kann sowohl im Frontend als auch im Backend geschehen. Der Prototyp wählt hier den Backend-Ansatz, damit die gleiche Verarbeitungskette sowohl für den Builder (bzw. das Admin-Tool), ein Dashboard oder eine Masterportalkomponente verwendet werden kann.
Nach erfolgreichem WPS-Prozess wird hierbei das Ergebnisobjekt abhängig von der zuvor erläuterten Chartkonfiguration in ein chart-js ChartData und ChartOptions Objekt umgewandelt, d.h. die Datensatz-struktur wird generiert, die Werte werden den Datensätzen zugeordnet, die Labels werden aus den Attributen erzeugt und die Styling-Operationen der Konfiguration werden auf die jeweiligen Felder übertragen. Die Struktur variiert dabei leicht je nach Chart-Typ, entspricht aber weitestgehend dem Beispiel aus 2.3.1. Im Prototyp sind gegenwärtig sind chart-typ-spezifische Unterschiede nur für Balken und Tortendiagramme hinreichend getestet und implementiert.

## Speichern und Einbetten
Einmal erzeugte Konfigurationen für Aggregationen, Charts und Filter sollen in einer Datenbank mit einem Hash abgelegt werden, über den sie zu einem beliebigen Zeitpunkt wieder abgerufen bzw. ausgeführt werden können. Wie zuvor erläutert werden Filter dabei unabhängig von den Aggregationen vorgehalten. Das Hashing geschieht bestenfalls nicht über einen Zufallsgenerator, sondern über ein akkurates Abbild des Konfigurationsobjekts (MD5, SHA, o.ä.), damit gleiche Konfigurationen nicht mehrfach gespeichert werden.
Die in der Datenbank gespeicherten Konfigurationen können mittels der Hashes wieder aus der Datenbank ausgelesen werden und die WPS-Prozesse direkt über einen HTTP-Request ausgeführt werden. So können beliebige Kombinationen gespeicherter Charts und Filter in Frontend-Applikationen wie Dashboards oder dem Masterportal eingebettet werden. Die Übergabe der Hashes geschieht dabei entweder in einem Konfigurationsobjekt (wie der config.json des Masterportals), exportiert aus dem Admin-Tool, oder über URL-Parameter.
Zum Beispiel:
https://geoportal-hamburg.de/dashboard?charts=f5f0535671ad2c979ba044e3fe24a5eb,f3a066faf06a41a7c18b0e5ad21c2859&filters=9eb8881987a2596a315242243f255322,bf91654f9bfa9cc8a6cffdec284ce8b6
So müsste im Idealfall nur eine einzige Anwendung deployed und gepflegt werden, die eine Vielfalt von Dashboards abbilden kann. Man könnte sich vorstellen, die Gesamtkonfiguration eines Dashboards aus Chart-, Filter- und Layout-Hashes (und perspektivisch MasterportalAPI Maps) mit einem benutzerdefinierten Namen abzuspeichern, sodass sich die URL eines Dashboards leicht lesbar als https://geoportal-hamburg.de/dashboard?id=mein-dashboard oder besser https://geoportal-hamburg.de/dashboard/mein-dashboard darstellen ließe.

## Konfiguationsdateien

Um Aggregationen (über ein GUI) konfigurieren und diese Konfigurationen später wiederverwenden zu können, müssen die in 4.1.1 beschriebenen ausgewählten Inputs für Daten und Filter in das XML-Format der WPS Request-Bodies übersetzt werden. Im Rahmen des Prototypen geschieht das via der Attribute „key“ und „inputType“, über die die konfigurierten Werte in das XML-Dokument eingefügt und die relevanten Tags erzeugt bzw. zugeordnet werden.
Die Arbeitsschritte folgen dabei der zuvor beschriebenen Logik und greifen im Wesentlichen auf folgende Ressourcen zurück:

### Aggregation-Template (JSON):

Die konfigurierbaren Parameter einer Aggregation: Das Objekt definiert url und processIdentifier des WPS; die URI des requestBody-Templates (XML); sowie Inputs und Filter. Das Template wird über das UI befüllt und an das Backend übersendet.
Beispiel gs:Aggregate:
```
{
        "name": "Nach Attribut aggregieren",
        "description": "Lorem Ipsum dolor sit amet"
        "service": "WPS",
        "url": "https://csl-lig.hcu-hamburg.de/geoserver/ows",
        "request": "EXECUTE",
        "version": "1.0.0",
        "processIdentifier": "gs:Aggregate",
        "requestBody": "gs_aggregate.xml",
		// Gibt an, dass 3 Inputs zu deklarieren sind
        "inputs": [
		// Für diesen Input sind die Werte vordefiniert.
		// Sie werden in einem Dropdown-Menü zur Auswahl angeboten
		// Der Input verweist nicht auf einen Datensatz.
            {
                "name": "Operation",
				// Der Key nach dem die Felder im XML gematched werden
                "key": "input_0",
                "items": [
                    "Sum",
                    "SumArea",
                    "Count",
                    "Average",
                    "Median",
                    "StdDev",
                    "Max",
                    "Min"
                ]
            },
			// Dieser Input verweist auf einen Datensatz (in der UDP oder als Rohdaten).
			// Das Attribute „type“ weißt darauf hin, dass für den Layer ein Attribut des angebenen
			// Datentyps angegeben werden muss.
            {
                "name": "zu aggregierendes Attribut",
				// Der Key nach dem die Felder im XML gematched werden
                "key": "input_1",
                "type": "number",
     			„value“: „“
            },
			// Dieser Input verweißt analog zu „input_1“ auf ein Attribut des Typs „string“
			// Das Attribute „linked“ weißt darauf hin, dass der Input-Layer für diesen Input derselbe ist, wie der aus des „input_1“
            {
                "name": "aggregieren nach...",
				// Der Key nach dem die Felder im XML gematched werden
                "key": "input_2",
                "type": "string",
                "linked": 1,
     			„value“: „“
            }
        ],
		// Gibt an, dass 1 Filter für die Aggregation vordefiniert werden kann.
		// Bezieht sich in diesem Fall auf den einzigen auszuwählenden Layer Input
		// Das Verhalten der Filter wird im Folgenden genauer beschrieben
        "filters": [
            {
                "name": "Layer Filter",
				// Der Key nach dem die Felder im XML gematched werden
                "key": "filter",
                "type": "CQL",
                "value": ""
            }
        ]
    }
```

Beispiel gs:Aggregate (befüllt, Auszug):

```
...
 "inputs": [       
 				{
                 “name": "zu aggregierendes Attribut",
				// Der Key nach dem die Felder im XML gematched werden
                "key": "input_1",
                "type": "number"
				// Die Werte für das Input-Element mit Key „input_1“, InputType „dataSource “
                "dataSource": {
                    "id": "3"
                    "name": "Kitas",
                    "url": "https://csl-lig.hcu-hamburg.de/geoserver/ows",
                    "typ": "WFS",
                    "featureType": "KitaEinrichtungen",
                    "outputFormat": "application/json",
                    "version": "1.1.0",
                    "featureNS": "builder"
                },
				// Der Wert für das Input-Element mit Key „input_1“, InputType „attribute“
                "value": "KapKindneu"
         },
         ...
]
...
```

### RequestBody -Template (XML):
Die Inputs für Daten, Layer-Attribute und Filter werden mithilfe der Attribute „key“ und „inputType“ zugewiesen. der „key“ entspricht dabei dem „key“ des Aggregations-Templates. Der Input-Type gibt an welches Attribut des spezifizierten Inputs hier verwendet werden soll.
Beispiel gs:Aggregate:
```
<?xml version="1.0" encoding="UTF-8"?>
<wps:Execute version="1.0.0" service="WPS">
    <!— processIdentifier, nicht variabel —>
    <ows:Identifier>gs:Aggregate</ows:Identifier>
    <wps:DataInputs>
        <!— Element für den Input mit Key „input_1“, verweist auf die „dataSource“.
        Wird ein Rohdaten-Objekt (z.B. GeoJSON) mitgegeben, wird im Backend das
        <wps:Reference>
        Element durch ein
        <wps:Data>
        Element ausgetauscht —>
        <wps:Input key="input_1" inputType="dataSource">
            <ows:Identifier>features</ows:Identifier>
            <!— im xlink:href-Attribut wird die URL des WFS angegeben —>
            <wps:Reference mimeType="text/xml" xlink:href="###input_1" method="POST">
                <wps:Body>
                    <wfs:GetFeature service="WFS" version="1.0.0" outputFormat="GML2" xmlns:builder="builder">
                        <!— im Query wird die Referenz zum Daten-Layer featureType hinterlegt —>
                        <wfs:Query typeName="###input_1/key"/>
                    </wfs:GetFeature>
                </wps:Body>
            </wps:Reference>
        </wps:Input>
        <!— Element für den Input mit dem Key „input_1“, verweist auf das ausgewählte Attribut des Layers („value“) —>
        <wps:Input key="input_1" inputType="attribute">
            <ows:Identifier>aggregationAttribute</ows:Identifier>
            <wps:Data>
                <wps:LiteralData>###input_1/value</wps:LiteralData>
            </wps:Data>
        </wps:Input>
        <!— Element für den Input mit dem Key „input_0“, verweist auf den gewählten Wert aus der vorgegebenen Liste —>
        <wps:Input key="input_0" inputType="attribute">
            <ows:Identifier>function</ows:Identifier>
            <wps:Data>
                <wps:LiteralData>###input_0/value</wps:LiteralData>
            </wps:Data>
        </wps:Input>
        <wps:Input>
            <ows:Identifier>singlePass</ows:Identifier>
            <wps:Data>
                <wps:LiteralData>True</wps:LiteralData>
            </wps:Data>
        </wps:Input>
        <!— Element für den Input mit dem Key „input_2“, verweist auf das ausgewählte Attribut des Layers („value“). Der
        Layer ist dabei derselbe wie bei „Input_1“, s. Aggregation-Template —>
        <wps:Input key="input_2" inputType="attribute">
            <ows:Identifier>groupByAttributes</ows:Identifier>
            <wps:Data>
                <wps:LiteralData>###input_2/value</wps:LiteralData>
            </wps:Data>
        </wps:Input>
        <!— Element für den Filter mit dem Key „filter“. Der CQL Wert des Filters wird hier eingeschrieben. Kann durch
        externe Filter nachträglich alteriert werden (s. unten). —>
        <wps:Input key="filter" inputType="filter">
            <ows:Identifier>filter</ows:Identifier>
            <wps:Data>
                <wps:ComplexData mimeType="text/plain; subtype=cql"><![CDATA[bezirk='Altona']]></wps:ComplexData>
            </wps:Data>
        </wps:Input>
    </wps:DataInputs>
    <wps:ResponseForm>
        <wps:RawDataOutput mimeType="application/json">
            <ows:Identifier>result</ows:Identifier>
        </wps:RawDataOutput>
    </wps:ResponseForm>
</wps:Execute>
```
### Chart-Konfiguration
Auch das Chart selbst kann im Frontend über das GUI konfiguriert werden. Dabei stehen in Dropdown-Menüs und Freitextfeldern alle Parameter zur Verfügung, die Bibliothek chart-js je nach Chart-Typ spezifiziert. Diese Spezifikationen sind in einer chart_options.json festgehalten. Die NutzerIn wählt dabei zunächst den Chart-Typ und gibt anschließend die weiteren Parameter ein.

Beispiel:
```
"chart": {
        "chartOptions": {
            "dataset": {
                "backgroundColor": ["red"]
            },
            "options": {
                  „cutoutPercentage“: 50
             }
        },
        "chartType": "pie"
    }  
```
Im Prinzip können alle 3 Bausteine (Die Aggregationskonfiguration, der RequestBody und die Chartkonfiguration) unabhängig voneinander oder in einem zusammenhängenden Objekt vorgehalten werden. im Prototyp wird eine Mischform verwendet, wobei Aggregations- und Chartkonfiguration gemeinsam an das Backend übersendet werden, dort der RequestBody zusammengefügt und nach erfolgreicher Aggregation das Ergebnis zusammen mit der Chartkonfiguration entweder direkt an den Client bzw. das Dashboard zurückgesendet wird oder vorher entsprechend des Charttyps in ein chart-js Objekt konvertiert wird. Mehr dazu in den Kapiteln 4.1.5 und 4.1.6
### Filter
Wie oben erläutert werden die Filter als Teil des Konfigurationsobjekts mit Key und CQL-Wert übergeben und auf Basis des Keys in das WPS XML eingefügt. Ein einfaches Beispiel könnte so aussehen:
```
{
    “value": "gattung_de='Eiche'",
                "key": "pointFilter",
                "name ": "Baumart",
}
```
Wie in 4.1 betrachtet ist es aber wünschenswert, einen Filter nicht einmalig konfigurieren zu müssen, sondern z.B. durch ein Filter-UI-Element in einem Dashboard eine oder bestenfalls direkt mehrere Aggregationen und Charts zur Laufzeit filtern zu können. Um das zu erreichen können sollen Filter unabhängig von den Aggregationen und Charts konfiguriert und zur Laufzeit (z.B. des Dashboards) basierend auf dem Key des Filters wieder auf die Aggregationen appliziert werden können. Dabei wird der Dienst dann mit den neuen Filter-Werten erneut aufgerufen und der RequestBody entsprechend angepasst.
So könnten zum Beispiel in einem Dashboard mit 2 Charts (Kitas pro Stadtteil, Kitas pro Träger) und 2 Filtern (Nach Bezirk filtern, Nach Kitagröße/Kapzität filtern) beide Filter beide Charts beeinflussen. Die Konfigurationsobjekte der Filter könnten zum Beispiel so aussehen:
Bsp. Bezirke filtern:
```
{
    "type": "select",
    "key": "areaFilter",
    "name": "Bezirke filtern",
    "attribute": "bezirk",
    "items ": ["Hamburg-Mitte", "Hamburg-Nord", "Eimsbüttel", "Altona", "Wandsbek", "Bergedorf", "Harburg"]
    "multiple": true 
}
```
Bsp. Kitas filtern:
```
{
    "type": "slider",
    "key": "pointFilter",
    "name": "Kitaplätze",
    "attribute": "KapKindneu",
    "minmax": [
        0,
        "1000"
    ],
    "step": "10"
}
```
Das Filterobjekt der Aggregationskonfigurationen könnte entsprechend so aussehen:
```
[
        {
            "key": "pointFilter",
            "value": "KindKap BETWEEN 0 AND 200"
        },
        {
            "key": "areaFilter",
            "value": "Bezirk IN ('Altona', 'Eimsbüttel')"
        }
    ]
```

Bei komplexeren Dashboards ist es voraussichtlich notwendig die Keys weiter zu differenzieren bzw. die Filter so konfigurieren zu können, dass sie auf mehrere Keys angewendet werden können, je nach Template der Aggregation. (z.B: "key": ["kitas:areaFilter", „einwohner:areaFilter“]).


## Architecture

Link to Schema: https://miro.com/app/board/o9J_khFZLbE=/

![Architecture_Schema](https://bitbucket.org/till-hcu/builder-back-end/downloads/architektur.png)


## Docker
We recommend using the respective `docker-compose` file in https://bitbucket.org/till-hcu/builder-back-end

For backend only run:
```docker image build -t dashboard-builder/docker-python-backend .
docker run -p 5000:5000 -e DOCKER=True -d --name docker-python-backend-1 dashboard-builder/docker-python-backend```